package org.stepdefinition;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "src/test/resources/features/testng.feature", glue = "org.stepdefinition",
monochrome = true)

public class TestNgRunner extends AbstractTestNGCucumberTests{

}
