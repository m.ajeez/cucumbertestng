package org.stepdefinition;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TestNgSteps {
	public static WebDriver driver;
	@Given("Open the website")
	public void open_the_website() {
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
	}

	@Then("The home page of the website get displayed")
	public void the_home_page_of_the_website_get_displayed() {
		driver.get("https://demowebshop.tricentis.com/");
	}

	@When("Click on the login profile button")
	public void click_on_the_login_profile_button() {
		driver.findElement(By.linkText("Log in")).click();
	}

	@When("Enter the email in the email field")
	public void enter_the_email_in_the_email_field() {
		driver.findElement(By.xpath("(//input[@id=\"Email\"])")).sendKeys("abdulajeez@gmail.com");
		
	}

	@When("Enter the password in the password field")
	public void enter_the_password_in_the_password_field() {
		driver.findElement(By.xpath("(//input[@id=\"Password\"])")).sendKeys("ajeez@123");
		
	}

	@When("click on the login button")
	public void click_on_the_login_button() {
		driver.findElement(By.xpath("(//input[@type=\"submit\"])[2]")).click();
	}

	@When("The home page with username should be displayed")
	public void the_home_page_with_username_should_be_displayed() {
		System.out.println("The home page with username should be displayed");
	}

	@When("click the logout button")
	public void click_the_logout_button() {
	driver.findElement(By.linkText("Log out")).click();
	}

	@Then("cthe user should taken to the homepage")
	public void cthe_user_should_taken_to_the_homepage() {
		System.out.println("The user should taken to the homepage");
	}

	@Then("close the browser")
	public void close_the_browser() {
	driver.quit();
	}

}
